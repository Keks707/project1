import math
import numpy





def stringInfo(s):
    print('number of commas per line: ' , (s.count(',')))
    print('number of spaces per line: ' , (s.count(' ')))
    print("number of characters per line: ", (len(s)))
    


def stringInfoUI():
    s = input("Enter string: ")
    stringInfo(s)





#main
def mainUI():
    while True:
        action = input("Enter action (math, string, matrix, exit)")
        if action == 'math':
            calcUI()
        elif action == 'string':
            stringInfoUI()
        elif action == 'matrix':
            matxUI()
        elif action == 'exit':
            break

mainUI()












