import math
import numpy


def calc(a, b, action):
    try: 
        if action in ('+', '-', '*', '/', 'celi', 'floor', 'sin', 'cos'):
            if action == '+':
                print("%.2f" % (a+b))
            

            elif action == 'celi':
                print("%.2f" % math.celi(a))
            
            elif action == 'floor':
                print("%.2f" % math.floor(a))
            
            elif action == 'sin':
                print("%.2f" % math.sin(a))
            
            elif action == 'cos':
                print("%.2f" % math.cos(a))
            
            elif action == '-':
                print("%.2f" % (a-b))
            
            elif action == '*':
                print("%.2f" % (a*b))
            
            elif action == '%':
                print("%.2f" % (a%b))
            
            elif action == '//':
                print("%.2f" % (a//b))

            elif action == '**':
                print("%.2f" % (a**b))

            elif action == '/':
                if b != 0:
                    print("%.2f" % (a/b))
                else:
                    print("Infinity")
        
    except KeyError as e:
        raise ValueError('Undefined unit: {}'.format(e.args[0]))





def stringInfoUI():
    s = input("Enter string: ")
    stringInfo(s)


def calcUI():
    a = int(input("Enter a: ")) 
    action = input("Choose action (+, -, *, /, %, //, **, celi, floor, sin, cos)")
    if action in ('+', '-', '*', '/', '%', '//', '**'):
        b = int(input("Enter b: "))
    else:
        b = 0
    calc(a, b, action)


#main
def mainUI():
    while True:
        action = input("Enter action (math, string, matrix, exit)")
        if action == 'math':
            calcUI()
        elif action == 'exit':
            break
mainUI()












